/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Samii-HS
 */
@Entity
@Table(name = "tabla_pagos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TablaPagos.findAll", query = "SELECT t FROM TablaPagos t")
    , @NamedQuery(name = "TablaPagos.findByIdPagos", query = "SELECT t FROM TablaPagos t WHERE t.idPagos = :idPagos")
    , @NamedQuery(name = "TablaPagos.findByNumPago", query = "SELECT t FROM TablaPagos t WHERE t.numPago = :numPago")
    , @NamedQuery(name = "TablaPagos.findByFechaPago", query = "SELECT t FROM TablaPagos t WHERE t.fechaPago = :fechaPago")
    , @NamedQuery(name = "TablaPagos.findBySaldo", query = "SELECT t FROM TablaPagos t WHERE t.saldo = :saldo")
    , @NamedQuery(name = "TablaPagos.findByAbono", query = "SELECT t FROM TablaPagos t WHERE t.abono = :abono")
    , @NamedQuery(name = "TablaPagos.findByPagado", query = "SELECT t FROM TablaPagos t WHERE t.pagado = :pagado")})
public class TablaPagos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pagos")
    private Integer idPagos;
    @Column(name = "num_pago")
    private BigInteger numPago;
    @Column(name = "fecha_pago")
    @Temporal(TemporalType.DATE)
    private Date fechaPago;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "saldo")
    private BigDecimal saldo;
    @Column(name = "abono")
    private BigDecimal abono;
    @Column(name = "pagado")
    private BigDecimal pagado;
    @JoinColumn(name = "id_estado_pago", referencedColumnName = "id_estado_pago")
    @ManyToOne
    private EstadoPago idEstadoPago;
    @JoinColumn(name = "id_forma_pago", referencedColumnName = "id_forma_pago")
    @ManyToOne
    private FormaPago idFormaPago;

    public TablaPagos() {
    }

    public TablaPagos(Integer idPagos) {
        this.idPagos = idPagos;
    }

    public Integer getIdPagos() {
        return idPagos;
    }

    public void setIdPagos(Integer idPagos) {
        this.idPagos = idPagos;
    }

    public BigInteger getNumPago() {
        return numPago;
    }

    public void setNumPago(BigInteger numPago) {
        this.numPago = numPago;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public BigDecimal getAbono() {
        return abono;
    }

    public void setAbono(BigDecimal abono) {
        this.abono = abono;
    }

    public BigDecimal getPagado() {
        return pagado;
    }

    public void setPagado(BigDecimal pagado) {
        this.pagado = pagado;
    }

    public EstadoPago getIdEstadoPago() {
        return idEstadoPago;
    }

    public void setIdEstadoPago(EstadoPago idEstadoPago) {
        this.idEstadoPago = idEstadoPago;
    }

    public FormaPago getIdFormaPago() {
        return idFormaPago;
    }

    public void setIdFormaPago(FormaPago idFormaPago) {
        this.idFormaPago = idFormaPago;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPagos != null ? idPagos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TablaPagos)) {
            return false;
        }
        TablaPagos other = (TablaPagos) object;
        if ((this.idPagos == null && other.idPagos != null) || (this.idPagos != null && !this.idPagos.equals(other.idPagos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.TablaPagos[ idPagos=" + idPagos + " ]";
    }
    
}
