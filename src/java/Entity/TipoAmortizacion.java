/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Samii-HS
 */
@Entity
@Table(name = "tipo_amortizacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoAmortizacion.findAll", query = "SELECT t FROM TipoAmortizacion t")
    , @NamedQuery(name = "TipoAmortizacion.findByIdAmortizacion", query = "SELECT t FROM TipoAmortizacion t WHERE t.idAmortizacion = :idAmortizacion")
    , @NamedQuery(name = "TipoAmortizacion.findByDescripcion", query = "SELECT t FROM TipoAmortizacion t WHERE t.descripcion = :descripcion")})
public class TipoAmortizacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_amortizacion")
    private Integer idAmortizacion;
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idTipoAmortizacion")
    private Collection<Prestamo> prestamoCollection;

    public TipoAmortizacion() {
    }

    public TipoAmortizacion(Integer idAmortizacion) {
        this.idAmortizacion = idAmortizacion;
    }

    public Integer getIdAmortizacion() {
        return idAmortizacion;
    }

    public void setIdAmortizacion(Integer idAmortizacion) {
        this.idAmortizacion = idAmortizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<Prestamo> getPrestamoCollection() {
        return prestamoCollection;
    }

    public void setPrestamoCollection(Collection<Prestamo> prestamoCollection) {
        this.prestamoCollection = prestamoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAmortizacion != null ? idAmortizacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoAmortizacion)) {
            return false;
        }
        TipoAmortizacion other = (TipoAmortizacion) object;
        if ((this.idAmortizacion == null && other.idAmortizacion != null) || (this.idAmortizacion != null && !this.idAmortizacion.equals(other.idAmortizacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.TipoAmortizacion[ idAmortizacion=" + idAmortizacion + " ]";
    }
    
}
