/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Samii-HS
 */
@Entity
@Table(name = "estado_pago")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EstadoPago.findAll", query = "SELECT e FROM EstadoPago e")
    , @NamedQuery(name = "EstadoPago.findByIdEstadoPago", query = "SELECT e FROM EstadoPago e WHERE e.idEstadoPago = :idEstadoPago")
    , @NamedQuery(name = "EstadoPago.findByDescripcion", query = "SELECT e FROM EstadoPago e WHERE e.descripcion = :descripcion")})
public class EstadoPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_estado_pago")
    private Integer idEstadoPago;
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(mappedBy = "idEstadoPago")
    private Collection<TablaPagos> tablaPagosCollection;

    public EstadoPago() {
    }

    public EstadoPago(Integer idEstadoPago) {
        this.idEstadoPago = idEstadoPago;
    }

    public Integer getIdEstadoPago() {
        return idEstadoPago;
    }

    public void setIdEstadoPago(Integer idEstadoPago) {
        this.idEstadoPago = idEstadoPago;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public Collection<TablaPagos> getTablaPagosCollection() {
        return tablaPagosCollection;
    }

    public void setTablaPagosCollection(Collection<TablaPagos> tablaPagosCollection) {
        this.tablaPagosCollection = tablaPagosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEstadoPago != null ? idEstadoPago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EstadoPago)) {
            return false;
        }
        EstadoPago other = (EstadoPago) object;
        if ((this.idEstadoPago == null && other.idEstadoPago != null) || (this.idEstadoPago != null && !this.idEstadoPago.equals(other.idEstadoPago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.EstadoPago[ idEstadoPago=" + idEstadoPago + " ]";
    }
    
}
