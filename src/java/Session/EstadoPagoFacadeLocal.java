/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.EstadoPago;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Samii-HS
 */
@Local
public interface EstadoPagoFacadeLocal {

    void create(EstadoPago estadoPago);

    void edit(EstadoPago estadoPago);

    void remove(EstadoPago estadoPago);

    EstadoPago find(Object id);

    List<EstadoPago> findAll();

    List<EstadoPago> findRange(int[] range);

    int count();
    
}
