/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.TipoAmortizacion;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Samii-HS
 */
@Local
public interface TipoAmortizacionFacadeLocal {

    void create(TipoAmortizacion tipoAmortizacion);

    void edit(TipoAmortizacion tipoAmortizacion);

    void remove(TipoAmortizacion tipoAmortizacion);

    TipoAmortizacion find(Object id);

    List<TipoAmortizacion> findAll();

    List<TipoAmortizacion> findRange(int[] range);

    int count();
    
}
