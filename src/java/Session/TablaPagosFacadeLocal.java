/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.TablaPagos;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Samii-HS
 */
@Local
public interface TablaPagosFacadeLocal {

    void create(TablaPagos tablaPagos);

    void edit(TablaPagos tablaPagos);

    void remove(TablaPagos tablaPagos);

    TablaPagos find(Object id);

    List<TablaPagos> findAll();

    List<TablaPagos> findRange(int[] range);

    int count();
    
}
